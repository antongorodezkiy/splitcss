$('document').ready(function() {
  
  /******************
    Tablet rotation
  ******************/
  
  var isiPad = navigator.userAgent.match(/iPad/i) != null;
  
  if(isiPad) {
    $('body').prepend('<div id="rotatedevice"><h1>Please rotate your device 90 degrees.</div>');
  }
  
  
 

  
    /************************
      Combined input fields
    ************************/
    
    $('p.combined input:last-child').addClass('last-child');
  
  
    /********************
      Pretty checkboxes
    ********************/
  
    $('input[type=checkbox], input[type=radio]').prettyCheckboxes();
  
    /**********************
      Pretty select boxes
    **********************/
  
    $('select').select_skin();
  
    /*********************
      Pretty file inputs
    *********************/

    //$('input[type="file"]').customFileInput();
    
	
	/*******
      Uploads
    *******/
	blockElement($("#sources_clear"));
	
	function blockElement(selector)
	{
		selector.attr("disabled","disabled");
		selector.attr("readonly","readonly");
		selector.attr("opacity","0.5");
	}
	
	function unblockElement(selector)
	{
		selector.removeAttr("readonly");
		selector.removeAttr("disabled");
		selector.removeAttr("opacity","1");
	}
	
	function readonlyElement(selector)
	{
		selector.attr("readonly","readonly");
		selector.attr("opacity","0.5");
	}
	
	function unreadonlyElement(selector)
	{
		selector.removeAttr("readonly");
		selector.removeAttr("opacity","1");
	}

		
		$("#source_url").change(function()
		{
			$.post(url['upload_remote'],{ "source" : $("#source_url").val() },function(data)
			{
				var json = $.parseJSON(data);
				
				notify(json);
				
				if (json.data != '' && json.data != null)
				{
					$("#source_textarea").val(json.data);
					
					readonlyElement($("#source_textarea"));
					blockElement($("#source_url"));
					blockElement($("#source_file"));
					$("#source_fileUploader").css("visibility","hidden");
					unblockElement($("#sources_clear"));
				}
				
				return false;
			});
		});
	
	
	
		
		$("#source_textarea").change(function()
		{
			blockElement($("#source_url"));
			blockElement($("#source_file"));
			unblockElement($("#sources_clear"));
			$("#source_fileUploader").css("visibility","hidden");
		});
		if ($("#source_textarea").text() != '' && $("#source_textarea").text() != undefined)
			$("#source_textarea").change();
		
		// разблокировка всего
		$("#sources_clear").click(function()
		{
			$.post(url['clear'],function(data)
			{
				var json = $.parseJSON(data);
				
				notify(json);
				
				if (!json[0].error || json[0].error == '' || json[0].error == undefined)
				{
					unreadonlyElement($("#source_textarea"));
					$("#source_textarea").val("");
					
					unblockElement($("#source_url"));
					$("#source_url").val("");
					
					unblockElement($("#source_file"));
					$("#source_file").val("");
					
					$("#source_fileUploader").css("visibility","visible");
					
					blockElement($("#sources_clear"));
				}
			});
		});
		
		
		
		// ajax direct
			$('#source_file').uploadify({
				'uploader'  : base_url+'js/uploadify.swf',
				'script'    : url['upload_direct'],
				'cancelImg' : base_url+'css/cancel.png',
				'auto'      : true,
				'buttonText': '*.css',
				'fileDesc'   : 'css',
				'fileExt'   : '*.css',
				'fileObjName': 'source_file',
				'onError'  : function(event,queueID,fileObj,response)
				{
					var json = $.parseJSON(response);
					notify(json);
					notifyError('Загрузка файла не удалась');
				},
				'onComplete'  : function(event,queueID,fileObj,response)
				{
					var json = $.parseJSON(response);
					notify(json);

					if (json.data != '' && json.data != null)
					{
						$("#source_textarea").val(json.data);
						
						readonlyElement($("#source_textarea"));
						blockElement($("#source_url"));
						blockElement($("#source_file"));
						$("#source_fileUploader").css("visibility","hidden");
						unblockElement($("#sources_clear"));
					}
				}
			  });

		
		
		
		
	/***********
      Functions
    ************/
		
		
		$("#functions form").submit(function()
		{
			var form = $(this);
			$.post(form.attr("action"),form.serialize(),function(data)
			{
				var json = $.parseJSON(data);
				
				notify(json);
				
				if (json.data != '' && json.data != null)
				{
					$("textarea[name='result']",form).val(json.data);
				}
				
				return false;
			});
			
			return false;
		});
		
		
    /*******
      Tabs
    *******/
  
    // Hide all .tab-content divs
    $('.tab-content').livequery(function() {
      $(this).hide();
    });

    // Show all active tabs
    $('.box-header ul li.active a').livequery(function() {
      var target = $(this).attr('href');
      $(target).show();
    });
  
    // Add click eventhandler
    $('.box-header ul li').livequery(function() {
      $(this).click(function() {
        var item = $(this);
        var target = item.find('a').attr('href');
        
        if ( target == '#source_code' )
		{
			load = true;
        }
		else if ( target == '#settings' )
		{			
			load = true
		}
		else if ( target == '#result_code' )
		{
			if( $("#source_textarea").val() == '')
			{
				notifyError('Загрузите исходный код');
				return false;
			}

			$.post(url['parse'],$("#settings select, #settings input, #source_textarea").serialize(),function(data)
			{
				var json = $.parseJSON(data);
				
				notify(json);
				
				if (json == '' || json == null)
				{
					notifyError('Код css не корректен');
					return false;
				}
				
				if (json.data != '' && json.data != null)
				{
					$.each(json.data,function(index,value)
					{
						console.log(index);
						$("#"+index+"_textarea").val(value);
					});
				}
				
				
			});
			
			
			load = true
		}
		else if ( target == '#functions' )
		{
			load = true;
		}
		else if ( target == '#about' )
		{
			load = true;
		}
		else if ( target == '#feedback' )
		{
			load = true;
		}
		else if ( target == '#save_the_world' )
		{
			load = true;
		}
		else if ( target == '#not_found' )
		{
			load = true;
		}
		else
		{
			if ( $(this).hasClass('noajax') )
				return true;
			else
			{
				notifyError('Не удается загрузить вкладку');
				load = false;
			}
        }
		
		$('.box-header ul li').removeClass("active");
		$('.tab-content').hide();
		$(target).show();
		$(this).addClass("active");
    
        return false;
      });
    });

    
	
	
		// Show all active tabs
		$('#result_code ul li a').click(function() {
			$('#result_code ul li').removeClass("active");
			$('#result_code .t_area').hide();
			var target = $(this).attr('href');
			$(target).show();
			$(this).parent().addClass("active");
			return false;
		});
		
		$('#result_code ul li a:first').click();
		
		
		// Show all active tabs
		$('#functions ul li a').click(function() {
			$('#functions ul li').removeClass("active");
			$('#functions .t_area').hide();
			var target = $(this).attr('href');
			$(target).show();
			$(this).parent().addClass("active");
			return false;
		});
		
		$('#functions ul li a:first').click();

	  
    /***********
      Tooltips
    ***********/
    
    $('.tooltip').tipsy();
	
	/***********
      Выделяем текст в input 
    ***********/
		$("textarea[readonly='readonly']").live("focus",function()
		{
			this.select();
		});
		
		$("textarea[readonly='readonly']").live("click",function()
		{
			this.select();
		});
	// выделяем текст в input
	
	$(".form_clear").click(function()
	{
		//$("textarea","body").val("");
	});


});
