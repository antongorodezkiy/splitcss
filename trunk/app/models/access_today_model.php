<?php

class Access_today_model extends CI_Model
{
	
	function __construct()
    {
        parent::__construct();
		
		$this->access = $this->load->database('access_db', true, true);

    }
	
	
	/*
	 *******************************************/

	/*
	** Добавление
	*/		
		function add()
		{
			$fields['ip_today'] = ip2long($this->input->ip_address());
			$fields['day_counter_timer_today'] = time();
			$fields['minute_counter_timer_today'] = time();
			$fields['per_day_counter_today'] = 1;
			$fields['per_minute_counter_today'] = 1;
			$fields['minute_block_today'] = 0;
			return $this->access->insert('today',$fields);
		}

	/*
	** Инкремент счетчиков
	*/
    function plusCounters()
    {
		$this->access->where('ip_today', ip2long($this->input->ip_address()));
		$this->access->set('per_day_counter_today', 'per_day_counter_today+1', FALSE);
		$this->access->set('per_minute_counter_today', 'per_minute_counter_today+1', FALSE);
		$result = $this->access->update('today');

		return $result;
    }
	
	/*
	** Блокировка за превышение минутного лимита
	*/
    function blockForMinuteLimitExceeded()
    {
		$this->access->where('ip_today', ip2long($this->input->ip_address()));
		$fields = array(
			'minute_block_today' => 1
		);
		return $this->access->update('today', $fields); 
    }
	
		/*
		** Разблокировка
		*/
		function unBlockForMinuteLimitExceeded()
		{
			$this->access->where('ip_today', ip2long($this->input->ip_address()));
			$fields = array(
				'minute_block_today' => 0,
				'per_minute_counter_today' => 1
			);
			$result = $this->access->update('today', $fields);
			
			return $result;
		}
	
	/*
	** Очистка минутного лимита
	** Для назаблокированных по минутному лимиту
	** для текущего ip
	** для тех, у кого сброс минутного лимита больше минуты
	*/
	function resetMinuteCounter()
    {
		$this->access->set('per_minute_counter_today', 0);
		$this->access->set('minute_counter_timer_today', time());
		
        $this->access->where('ip_today', ip2long($this->input->ip_address()));
		$this->access->where('minute_counter_timer_today <', (time() - 60)); // забыть все старое
		$result = $this->access->update('today');
		//die($this->access->last_query());
		return $result;
    }

		/*
		** Удаление по истечению срока годности
		*/
		function deleteByExpire($expire_time = 86400)
		{
			$where = array(
				'day_counter_timer_today <' => (time() - $expire_time),
			);
			return $this->access->delete('today', $where);
		}
	
	
	/*
	** Возвращает, какой лимит превышен
	*/
	function isLimitExceeded($day_query_limit = 3000,$minute_query_limit = 60)
	{
		// если 
		$this->access->where('per_day_counter_today >', $day_query_limit);
		$this->access->where('per_minute_counter_today >', $minute_query_limit);
		
		$statistics = $this->loadByIp();
				 
		
	}
	
	/*
	** Загрузка счетчиков по ip
	*/
	function loadByIp()
	{
		$query = $this->access->select('*')
				 ->from('today')
				 ->where('ip_today', ip2long($this->input->ip_address()))
				 ->get();
		
		if ($query->num_rows())
		{
			return $query->row();
		}
		else
			return false;
	}
	

}