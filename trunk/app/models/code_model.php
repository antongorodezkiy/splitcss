<?php

class Code_model extends CI_Model
{
	
	function __construct()
    {
        parent::__construct();
    }
	
	
	/*
	 *******************************************/

	/*
	** Добавление
	*/		
		function addAllLoop($fields_array)
		{
			$this->db->trans_begin();
			foreach($fields_array as $fields)
				$this->db->insert('css',$fields);
				
			return $this->db->trans_commit();
		}

	/*
	** Редактирование
	*/
    function edit($fields,$id)
    {
		$this->db->where('id_css', $id);
		return $this->db->update('css', $fields); 
    }
	
	/*
	** Удаление по сессии
	*/
	function deleteAllBySession()
    {
        return $this->db->delete('css', array( 'session_css' => session_id() ));
    }
	
	/*
	** Удаление по сессии
	*/
	function deleteResultsBySession()
    {
        return $this->db->delete('css', array( 'session_css' => session_id(), 'type_css !=' => 'source' ));
    }
	
		/*
		** Удаление по истечению срока годности
		*/
		function deleteByExpire($expire_time = 36000)
		{
			return $this->db->delete('css', array( 'time_css <' => (time() - $expire_time) ));
		}
	
	
	/*
	** Владелец ли текущий пользователь
	*/
	function isOwner($id)
	{
		$query = $this->db->select('session_css')
				 ->from('css')
				 ->where('id_css',$id)
				 ->where('session_css',session_id())
				 ->get();
				 
		if ($query->row()->session_css)
		{
			return true;
		}
		else
			return false;
	}
	
	/*
	** Список имен и айдишников для выпадающих списков
	*/
	function loadBySession($load_what = '')
	{
		$query = $this->db->select('*')
				 ->from('css')
				 ->where('session_css',session_id())
				 ->get();
		
		if ($query->num_rows())
		{
			return $query->result_array();
		}
		else
			return false;
	}
	
	/*
	** Загрузка только источника
	*/
	function loadSource()
	{
		$this->db->where('type_css','source');
		$result = $this->loadBySession();
		return $result ? $result[0]['code_css'] : '' ;
	}
	
	/*
	** Загрузка только форматированного
	*/
	function loadFormated()
	{
		$this->db->where('type_css','formated');
		$result = $this->loadBySession();
		return $result ? $result[0]['code_css'] : '' ;
	}
	
	/*
	** Загрузка только результатов
	*/
	function loadResults()
	{
		$this->db->where('type_css !=','source');
		return $this->loadBySession();
	}
	
	/*
	** Загрузка только результатов
	*/
	function loadSplittedResults()
	{
		$this->db->where('type_css !=','source');
		$this->db->where('type_css !=','formated');
		return $this->loadBySession();
	}

}