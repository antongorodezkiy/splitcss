<?
/*
The reCaptcha server keys and API locations

Obtain your own keys from:
http://www.recaptcha.net
*/
$config['recaptcha'] = array(
  //'public'=>'6LeEc80SAAAAAKrJ1deiVPltZkZ0HHDdoviD3P2k', // splitcss.com
  //'private'=>'6LeEc80SAAAAAPapQVoG4w8owRvQovjwAWo--sUP',	// splitcss.com
	'public'=>'6Lc4dc0SAAAAAE0mk2GN9DCXP2iviNd70p_XJXBX',
	'private'=>'6Lc4dc0SAAAAAGDhoytBk34bMxi25ygKvkW5Z-O3',
	'RECAPTCHA_API_SERVER' =>'http://www.google.com/recaptcha/api',
	'RECAPTCHA_API_SECURE_SERVER'=>'https://www.google.com/recaptcha/api',
	'RECAPTCHA_VERIFY_SERVER' =>'www.google.com',
	'RECAPTCHA_SIGNUP_URL' => 'https://www.google.com/recaptcha/admin/create',
	'theme' => 'clean'
);
