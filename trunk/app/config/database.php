<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'code_db';
$active_record = TRUE;

$db['code_db']['hostname'] = '';
$db['code_db']['database'] = APPPATH.'db/code.sqlite3';
$db['code_db']['busy_timeout'] = '50';
//$db['default']['username'] = 'root';
//$db['default']['password'] = 'root';
$db['code_db']['dbdriver'] = 'sqlite3';
$db['code_db']['dbprefix'] = '';
$db['code_db']['pconnect'] = FALSE;
$db['code_db']['db_debug'] = TRUE;
$db['code_db']['cache_on'] = FALSE;
$db['code_db']['cachedir'] = '';
$db['code_db']['char_set'] = 'utf8';
$db['code_db']['dbcollat'] = 'utf8_general_ci';
$db['code_db']['swap_pre'] = '';
$db['code_db']['autoinit'] = true;
$db['code_db']['stricton'] = FALSE;


$db['access_db']['hostname'] = '';
$db['access_db']['database'] = APPPATH.'db/accesses.sqlite3';
$db['access_db']['busy_timeout'] = '50';
//$db['default']['username'] = 'root';
//$db['default']['password'] = 'root';
$db['access_db']['dbdriver'] = 'sqlite3';
$db['access_db']['dbprefix'] = '';
$db['access_db']['pconnect'] = FALSE;
$db['access_db']['db_debug'] = TRUE;
$db['access_db']['cache_on'] = FALSE;
$db['access_db']['cachedir'] = '';
$db['access_db']['char_set'] = 'utf8';
$db['access_db']['dbcollat'] = 'utf8_general_ci';
$db['access_db']['swap_pre'] = '';
$db['access_db']['autoinit'] = true;
$db['access_db']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */