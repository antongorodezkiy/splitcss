<?php

// (c) http://www.phpclasses.org/browse/file/24561.html

class testUrl
{
        var $vector = array();

        function __construct($time = '10'){
            set_time_limit($time);
        }

        function addList($url){
          $this->vector[] = $url;
        }

        function returnList(){
          $list = $this->vector;
          // var_dump($list);
           for($i = 0; $i < count($list);$i++){
             $this->test($list[$i]);
           }
        }

        function test($url) {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          $output = curl_exec($ch);
          $return = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          $errors = array("", "400", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "411", "412", "413", "414", "415", "500", "501", "502", "503", "504", "505");

          if(in_array($return, $errors)) {
			return false;
          }
          else {
			return true;
            }
            curl_close($ch);
        }

} 