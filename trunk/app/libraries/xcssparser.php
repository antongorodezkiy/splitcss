<?php

class xcssparser extends cssparser
{
	private $removedProperties = array(), $removedSelectors = array(), $braceMode = "\n";
	public $orderSelectors = false, $orderProperties = false;
	
/*
** Переопределения
*/
	
	
	/*
	** Переопределение Clear
	*/
		function Clear()
		{
			parent::Clear();
			
			$this->removedProperties = array();
			$this->removedSelectors = array();
			$this->orderSelectors = false;
			$this->orderProperties = false;
		}
	
	/*
	** Переопределение ParseStr
	*/
		function ParseStr($str)
		{
			$this->Clear();
			
			// Remove comments
			if ($this->removeComments)
				$str = preg_replace("/\/\*(.*)?\*\//Usi", "", $str);
			
			// Parse this damn csscode
			$parts = explode("}",$str);
			if(count($parts) > 0) {
			  foreach($parts as $part) {
				if(strlen( trim($part) ) > 0)
				{
					
					list($keystr,$codestr) = explode("{",$part);
					$keys = explode(",",trim($keystr));
					if(count($keys) > 0) {
					  foreach($keys as $key) {
						if(strlen($key) > 0) {
						  $key = str_replace("\n", "", $key);
						  $key = str_replace("\\", "", $key);
						  $this->Add($key, trim($codestr));
						}
					  }
					}
				}
			  }
			}
			//
			return (count($this->css) > 0);
		}
  
	/*
	** Переопределение получения css
	*/
		function getCSS()
		{
			$result = "";
			foreach($this->css as $key => $values)
			{
				$result .= $key.$this->braceMode."{\n";
				foreach($values as $key => $value)
				{
					$result .= "\t$key: $value;\n";
				}
				$result .= "}\n\n";
			}
			return $result;
		}
	
	
	
/*
** Режимы
*/
  
	/*
	** Режим положения открывающей скобки
	*/
		function setBraceMode($mode = 'newline')
		{
			if ($mode == 'newline')
			{
				$this->braceMode = "\n";
			}
			else
				$this->braceMode = " ";
		}
	
	
	
	
	
	
/*
** PRIVATE, внутренняя магия)
*/
	
		/*
		** Выбрать только свойства, с определенным именем значения
		*/
			private function GetCSS_WithSelectiveProperties($selectedProperties, $mode = 'cut')
			{
		
				$this->removedProperties[] = $selectedProperties;
				$result = "";
		
				foreach($this->css as $selector => $values)
				{
					$properties = '';
					foreach($values as $key => $value)
					{
						$allowed = false;
						foreach($selectedProperties as $selectedProperty => $selectedPropertyValue)
						{
							if ( (strstr($key,$selectedProperty) or $selectedProperty == '*') && (strstr($value,$selectedPropertyValue) or $selectedPropertyValue == '*') )
							{
								$allowed = true;
								break;
							}
						}
								
						if ($allowed)
							$properties .= "\t$key: $value;\n";
					}
					
					if ($properties)
						$result .= $selector.$this->braceMode."{\n".$properties."}\n\n";
				}
				return $result;
			}
		
		
		
	
	
	
	
		/*
		** Выбрать только селекторы, с определенным именем значения
		*/
			private function GetCSS_WithSelectiveSelectors($selectedSelector, $mode = 'cut')
			{
		
				$this->removedSelectors[] = $selectedSelector;
				$result = "";
		
				foreach($this->css as $selector => $values)
				{
					$properties = '';
					foreach($values as $key => $value)
					{
						$properties .= "\t$key: $value;\n";
					}
					
					foreach($selectedSelector as $type => $name)
						if ( (strstr($selector, $name) or $name == '*') && ($selector[0] == $type or $type == '*') )
							$result .= $selector.$this->braceMode."{\n".$properties."}\n\n";
				}
				return $result;
			}
	
	
	

	/*
	** Дополнить вендорным border-radius
	** подразумевается, что передается css очищенный от вендорных свойств
	*/
		private function crossbrowser_BorderRadius($split = false)
		{
			
			$result = "";
	
			foreach($this->css as $selector => $values)
			{
				$properties = '';
				foreach($values as $key => $value)
				{
					
					if (strstr($key,'border-radius'))
					{
						unset($this->css[$selector][$key]);
						
						// разделять или не разделять на отдельные углы
						if ($split)
						{
							$this->css[$selector]['-moz-border-radius-topleft'] = $value;
							$this->css[$selector]['-webkit-border-top-left-radius'] = $value;
							$this->css[$selector]['-khtml-border-top-left-radius'] = $value;
							$this->css[$selector]['border-top-left-radius'] = $value;
							
							$this->css[$selector]['-moz-border-radius-topright'] = $value;
							$this->css[$selector]['-webkit-border-top-right-radius'] = $value;
							$this->css[$selector]['-khtml-border-top-right-radius'] = $value;
							$this->css[$selector]['border-top-right-radius'] = $value;
						
							$this->css[$selector]['-moz-border-radius-bottomright'] = $value;
							$this->css[$selector]['-webkit-border-bottom-right-radius'] = $value;
							$this->css[$selector]['-khtml-border-bottom-right-radius'] = $value;
							$this->css[$selector]['border-bottom-right-radius'] = $value;

							$this->css[$selector]['-moz-border-radius-bottomright'] = $value;
							$this->css[$selector]['-webkit-border-bottom-right-radius'] = $value;
							$this->css[$selector]['-khtml-border-bottom-right-radius'] = $value;
							$this->css[$selector]['border-bottom-right-radius'] = $value;

							
						}
						else
						{
							$this->css[$selector]['-moz-border-radius'] = $value;
							$this->css[$selector]['-webkit-border-radius'] = $value;
							$this->css[$selector]['-khtml-border-radius'] = $value;
							$this->css[$selector][$key] = $value;
						}
					}
					elseif (strstr($key,'border-bottom-right-radius'))
					{
						unset($this->css[$selector][$key]);
						$this->css[$selector]['-moz-border-radius-bottomright'] = $value;
						$this->css[$selector]['-webkit-border-bottom-right-radius'] = $value;
						$this->css[$selector]['-khtml-border-bottom-right-radius'] = $value;
						$this->css[$selector][$key] = $value;
					}
					elseif (strstr($key,'border-bottom-left-radius'))
					{
						unset($this->css[$selector][$key]);
						$this->css[$selector]['-moz-border-radius-bottomleft'] = $value;
						$this->css[$selector]['-webkit-border-bottom-left-radius'] = $value;
						$this->css[$selector]['-khtml-border-bottom-left-radius'] = $value;
						$this->css[$selector][$key] = $value;
					}
					elseif (strstr($key,'border-top-right-radius'))
					{
						unset($this->css[$selector][$key]);
						$this->css[$selector]['-moz-border-radius-topright'] = $value;
						$this->css[$selector]['-webkit-border-top-right-radius'] = $value;
						$this->css[$selector]['-khtml-border-top-right-radius'] = $value;
						$this->css[$selector][$key] = $value;
					}
					elseif (strstr($key,'border-top-left-radius'))
					{
						unset($this->css[$selector][$key]);
						$this->css[$selector]['-moz-border-radius-topleft'] = $value;
						$this->css[$selector]['-webkit-border-top-left-radius'] = $value;
						$this->css[$selector]['-khtml-border-top-left-radius'] = $value;
						$this->css[$selector][$key] = $value;
					}
					
				}
				
			}
			return true;
		}
		
		
		
	/*
	** Дополнить вендорным opacity
	** подразумевается, что передается css очищенный от вендорных свойств
	*/
		private function crossbrowser_Opacity()
		{
			
			$result = "";
	
			foreach($this->css as $selector => $values)
			{
				$properties = '';
				foreach($values as $key => $value)
				{
					
					if (strstr($key,'opacity'))
					{
						unset($this->css[$selector][$key]);
						
						$this->css[$selector]['-ms-filter'] = 'progid:DXImageTransform.Microsoft.Alpha(Opacity='.($value*100).')';
						$this->css[$selector]['filter'] = 'alpha(opacity='.($value*100).')';
						$this->css[$selector]['-moz-opacity'] = $value;
						$this->css[$selector]['-khtml-opacity'] = $value;
						$this->css[$selector][$key] = $value;
						
					}
					
					
				}
				
			}
			return true;
		}
	
	
	
	/*
	** Дополнить тени текста для IE
	*/
		private function crossbrowser_TextShadow()
		{
			
			$result = "";
	
			foreach($this->css as $selector => $values)
			{
				$properties = '';
				foreach($values as $key => $value)
				{
					
					if (strstr($key,'text-shadow'))
					{
					
						unset($this->css[$selector][$key]);

						$values = explode(' ',str_replace('px','',$value));
						
						$this->css[$selector]['-ms-filter'] = 'progid:DXImageTransform.Microsoft.DropShadow(Color='.$values[3].', OffX='.$values[0].', OffY='.$values[1].')';
						$this->css[$selector]['filter'] = 'progid:DXImageTransform.Microsoft.DropShadow(Color='.$values[3].', OffX='.$values[0].', OffY='.$values[1].')';
						$this->css[$selector][$key] = $value;
		
					}
					
					
				}
				
			}
			return true;
		}
		
		
	
	/*
	** Дополнить тени блока
	*/
		private function crossbrowser_BoxShadow()
		{
			
			$result = "";
	
			foreach($this->css as $selector => $values)
			{
				$properties = '';
				foreach($values as $key => $value)
				{
					
					if (strstr($key,'box-shadow'))
					{
					
						unset($this->css[$selector][$key]);

						$values = explode(' ',str_replace('px','',$value));
						$x = $values[0];
						$y = $values[1];
						$blur = $values[2];
						$color = $values[3];
						
						/*if ($x == 0 && $y == 0)
						{
							$direction = array( 90, 180, 270, 359 );
						}
						elseif ($x > 0 && $y == 0)
						{
							$direction = array( 90 );
						}
						elseif ($x > 0 && $y > 0)
						{
							$direction = array( 90, 180 );
						}
						elseif ($x == 0 && $y > 0)
						{
							$direction = array( 180 );
						}
						elseif ($x < 0 && $y > 0)
						{
							$direction = array( 180, 270 );
						}
						elseif ($x < 0 && $y == 0)
						{
							$direction = array( 270 );
						}
						elseif ($x < 0 && $y < 0)
						{
							$direction = array( 270, 359 );
						}
						elseif ($x == 0 && $y < 0)
						{
							$direction = array( 359 );
						}
						
						$this->css[$selector]['filter'] = 'progid:DXImageTransform.Microsoft.Blur(pixelradius='.$blur.')'."\n";
						foreach($direction as $dir)
							$this->css[$selector]['filter'] .= "\t\t".'progid:DXImageTransform.Microsoft.Shadow(color="'.$color.'", Direction='.$dir.', Strength='.$blur.')'."\n";
						*/
						$this->css[$selector]['-moz-box-shadow'] = $value;
						$this->css[$selector]['-webkit-box-shadow'] = $value;
						$this->css[$selector][$key] = $value;
		
					}
					
					
				}
				
			}
			return true;
		}
		
	
/*
** Интерфейс
*/	
		/*
		** Выбрать только свойства, которые не исключены
		*/
		public function getRestOnly()
		{
			$result = "";
			foreach($this->css as $selector => $values)
			{
				
				$properties = '';
				foreach($values as $key => $value)
				{
					$allowed = true;
					if ( count($this->removedProperties) )
					foreach($this->removedProperties as $removedProperties)
					{
						foreach($removedProperties as $removedProperty => $removedPropertyValue)
						{
							if ( (strstr($key,$removedProperty) or $removedProperty == '*') && (strstr($value,$removedPropertyValue) or $removedPropertyValue == '*') )
							{
								$allowed = false;
								break;
							}
						}
					}
							
					if ($allowed)
						$properties .= "\t$key: $value;\n";
				}

				if ($properties)
				{
					$allowed = true;
					if ( count($this->removedSelectors) )
					foreach($this->removedSelectors as $removedSelectors)
						foreach($removedSelectors as $type => $name)
							if ( (strstr($selector, $name) or $name == '*') && ($selector[0] == $type or $type == '*') )
							{
								$allowed = false;
								break;
							}
							
					if ($allowed)		
						$result .= $selector.$this->braceMode."{\n".$properties."}\n\n";
				}

			}
			return $result;
		}
		
		
		
		
		
		
		
		
		
		/* INTERFACE */
			function getColorsOnly($mode = 'cut')
			{
				// color, backgroud-color, border-color
				return $this->GetCSS_WithSelectiveProperties( array('color' => '*', '*' => '#') );
			}
			
			function getFontsOnly($mode = 'cut')
			{
				// font
				return $this->GetCSS_WithSelectiveProperties( array('font' => '*') );
			}
			
			function getImagesOnly($mode = 'cut')
			{
				// url
				$settings = array(
					'background' => 'url',
					'list' => 'url',
					'border' => 'url',
					'content' => 'url'
				);
				return $this->GetCSS_WithSelectiveProperties( $settings );
			}
	
			function getPseudoOnly($mode = 'cut')
			{
				// :hover, :active
				return $this->GetCSS_WithSelectiveSelectors( array('*' => ':') );
			}
			
			function getVendorsOnly($mode = 'cut')
			{
				// -moz-, -o-, -webkit-, -khtml-, filter:
				return $this->GetCSS_WithSelectiveProperties( array(
					'-moz-' => '*',
					'-o-' => '*',
					'-webkit-' => '*',
					'-ms-' => '*',
					'*' => 'filter:',
					'-khtml-' => '*',
					'*' => 'expression',
					'*' => 'icrosoft',
					'-pie-' => '*',
					'behavior' => '*'
				));
			}
			
			function getFilteredOnly($name='*', $value='*')
			{

				// array('*' => ':')
				return $this->GetCSS_WithSelectiveSelectors( array($name => $value) );
			}
			
	
	/* CSS3 */
			function makeCrossbrowserGradient()
			{
				$properties = array(
					'*' => '-gradient',
					'*' => 'Microsoft.Gradient',
				);
				$css = $this->GetCSS_WithSelectiveProperties( $properties );
				print_r($css);
				$this->ParseStr('gradient { '.$css.'}');
				
				print_r($this->css);
				die('iml');
				
				
				
				return $crossbrowser_css3_source;
			}
			
			
			function makeCrossbrowserBorderRadius($split_per_corner)
			{
				// вырежем вендорные свойства
				$properties = array(
					'-moz-border' => '*',
					'-webkit-border' => '*',
					'-khtml-border' => '*',
				);
				$this->GetCSS_WithSelectiveProperties( $properties );
				
				// получили безвендорный css
				$vendorless_css = $this->getRestOnly();
				
				// заново распарсили результат
				$this->ParseStr($vendorless_css);
				
				// получили без border css
				$this->crossbrowser_BorderRadius($split_per_corner);
			
				return $this->getRestOnly();
			}
			
		/* Text-shadow */
			function makeCrossbrowserTextShadow()
			{
				// вырежем вендорные свойства
				$properties = array(
					'*' => 'DropShadow'
				);
				$this->GetCSS_WithSelectiveProperties( $properties );
				
				// получили безвендорный css
				$vendorless_css = $this->getRestOnly();
				
				// заново распарсили результат
				$this->ParseStr($vendorless_css);
				
				// получили без border css
				$this->crossbrowser_TextShadow();
			
				return $this->getRestOnly();
			}
			
			
		/* Box-shadow */
			function makeCrossbrowserBoxShadow()
			{
				// вырежем вендорные свойства
				$properties = array(
					'*' => 'Shadow',
					'-moz-box-shadow' => '*',
					'-webkit-box-shadow' => '*'
				);
				$this->GetCSS_WithSelectiveProperties( $properties );
				
				// получили безвендорный css
				$vendorless_css = $this->getRestOnly();
				
				// заново распарсили результат
				$this->ParseStr($vendorless_css);
				
				// получили без border css
				$this->crossbrowser_BoxShadow();
			
				return $this->getRestOnly();
			}
			
		
		/* Opacity */
			function makeCrossbrowserOpacity()
			{
				// вырежем вендорные свойства
				$properties = array(
					'filter' => 'pacity',
					'-moz-opacity' => '*',
					'-khtml-opacity' => '*',
				);
				$this->GetCSS_WithSelectiveProperties( $properties );
				
				// получили безвендорный css
				$vendorless_css = $this->getRestOnly();

				// заново распарсили результат
				$this->ParseStr($vendorless_css);
				
				// получили без border css
				$this->crossbrowser_Opacity();
			
				return $this->getRestOnly();
			}
			
			
		/*
		** Режим реструктуризации вендорных свойств
		*/
			function restructureVendorsProperties()
			{
				// вырежем вендорные свойства
				$properties = array(
					'filter' => 'pacity',
					'-moz-opacity' => '*',
					'-khtml-opacity' => '*',
					'*' => '.Shadow',
					'-moz-box-shadow' => '*',
					'-webkit-box-shadow' => '*',
					'*' => '.DropShadow',
					'-moz-border' => '*',
					'-webkit-border' => '*',
					'-khtml-border' => '*',
					'*' => '-gradient',
					'*' => 'Microsoft.Gradient'
				);
				$this->GetCSS_WithSelectiveProperties( $properties );
				
				// получили безвендорный css
				$vendorless_css = $this->getRestOnly();

				// заново распарсили результат
				$this->ParseStr($vendorless_css);
				
					$this->crossbrowser_BorderRadius();
				
					$this->crossbrowser_TextShadow();
				
					$this->crossbrowser_BoxShadow();
					
					$this->crossbrowser_Opacity();
			
				return $this->getRestOnly();
			}
		
		
			
		/*
		** Сортировать селекторы 
		*/
			public function sortSelectors()
			{
				ksort($this->css);
			}
			
		/*
		** Сортировать свойства 
		*/
			public function sortProperties()
			{
				foreach($this->css as $selector => $values)
					ksort($this->css[$selector]);
			}
			

}