<html lang="ru" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru"><head>
	<title>404: Страница не найдена - ЭлКОР Сервис</title>
	
	<meta content="text/html;charset=UTF-8" http-equiv="content-type">
	<meta content="ru" http-equiv="content-language">
	<meta content="elcor" name="copyright">
	
	<link media="all" href="/css/reset.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/fluid.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/style.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/typography.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/forms.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/uniform.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/blue.css" type="text/css" rel="stylesheet">
	
	<link media="all" href="/css/simplestyle.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/login.css" type="text/css" rel="stylesheet">
	<link media="all" href="/css/jquery-ui.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript">
		// php2js config
		var base_url = "/";
		var controller = "login";
	</script>

	<script src="/js/jquery.js" type="text/javascript"></script>
	<script src="/js/jquery-ui.js" type="text/javascript"></script>
	<script src="/js/jquery.wysiwyg.js" type="text/javascript"></script>
	<script src="/js/jquery.uniform.js" type="text/javascript"></script>
	<script src="/js/jquery.placeholder.js" type="text/javascript"></script>
	
		<script type="text/javascript">
			function notify(json)
			{
				var now = new Date();
				now = now.getTime();
				for (key in json)
					$(".notify").prepend("&lt;div time=\""+now+"\" class=\"notice "+json[key].type+"\"&gt;&lt;p&gt;"+json[key].message+"&lt;/p&gt;&lt;/div&gt;");
					
				$(".notice").click(function(){ $(this).fadeOut(300); });
			}
			
			function close_old_notifies()
			{
				var now = new Date();
				now = now.getTime();
				$(".notice",".notify").each(function()
				{
					var notice_time = $(this).attr("time");
					
					if ( (now-notice_time) > 5000 )
						$(this).fadeOut(1000);
				});
			}
			
			$(document).ready(function()
			{
				setInterval("close_old_notifies()",5000);
				
				$(".notice",".notify").hover(function()
				{
					$(this).css("opacity","1");
				},
				function()
				{
					$(this).css("opacity","0.8");
				});
			});
		</script>
			<script src="/js/theme.js" type="text/javascript"></script>
	<script src="/js/main.js" type="text/javascript"></script>
	
</head>
<body class="login">
	<div id="wrap" class="container_24">
		
		<div class="waiter hidden">
			<div class="center"><img alt="Идет загрузка..." src="/img/waiter.gif"></div>
		</div>
		
		
		<div class="greed_24">
			<div class="notify">
			</div>
			<div class="login box_content">
				<h1>ЭлКОР Сервис</h1>
				<h2>404: Страница не найдена</h2>
				<form action="/" method="get">
					<div>
						<button type="submit" class="button">На главную</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</body>
</html>