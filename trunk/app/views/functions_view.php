<div class="tab-content results functions" id="functions">
	<h2>Функции</h2>
	
	<ul>
		<!--<li><a href="#css3_gradient">Gradient</a></li>-->
		<li class="active"><a href="#css3_border-radius">Border-radius</a></li>
		<li><a href="#css3_text-shadow">Text-shadow</a></li>
		<li><a href="#css3_box-shadow">Box-shadow</a></li>
		<li><a href="#css3_opacity">Opacity</a></li>
	</ul>
	
	<?php /*
	<!--Gradient-->
		<div id="css3_gradient" class="t_area">
			<form action="<?=site_url('parse/gradient')?>" method="post">
				<h4>Gradient</h4>
				
				<p>
					<label>Source:</label>
					<textarea name="source" placeholder="background: ..."></textarea>
				</p>
				
				<p>
					<label>Result:</label>
					<textarea readonly="readonly" name="result" placeholder="Нет свойств"></textarea>
				</p>
				
				<p>
					<button class="blue button" type="submit">Сделать п***ато</button>
				</p>
			</form>
		</div>
	*/ ?>
	
	<!--Border-radius-->
		<div id="css3_border-radius" class="t_area">
			<form action="<?=site_url('parse/border_radius')?>" method="post">
				<h4>Border-radius</h4>
				
				<p>
					<label>Source:</label>
					<textarea name="source" placeholder="border-radius: ..."></textarea>
				</p>
				
				<p>
					<label>Result:</label>
					<textarea readonly="readonly" name="result" placeholder="Нет свойств"></textarea>
				</p>
				
				<p>
					<input type="checkbox" name="split_per_corner" id="split_per_corner" value="1" />
					<label for="split_per_corner">Разделить по углам</label>
				</p>
				
				<p>
					<button class="blue button" type="submit">Сделать пи***то</button>
				</p>
			</form>
		</div>
	
	<!--Text-shadow-->
		<div id="css3_text-shadow" class="t_area">
			<form action="<?=site_url('parse/text_shadow')?>" method="post">
				<h4>Text-shadow</h4>
				
				<p>
					<label>Source:</label>
					<textarea name="source" placeholder="text-shadow: ..."></textarea>
				</p>
				
				<p>
					<label>Result:</label>
					<textarea readonly="readonly" name="result" placeholder="Нет свойств"></textarea>
				</p>

				
				<p>
					<button class="blue button" type="submit">Сделать пи***то</button>
				</p>
			</form>
		</div>
	
	<!--Box-shadow-->
		<div id="css3_box-shadow" class="t_area">
			<form action="<?=site_url('parse/box_shadow')?>" method="post">
				<h4>Box-shadow</h4>
				
				<p>
					<label>Source:</label>
					<textarea name="source" placeholder="box-shadow: ..."></textarea>
				</p>
				
				<p>
					<label>Result:</label>
					<textarea readonly="readonly" name="result" placeholder="Нет свойств"></textarea>
				</p>

				
				<p>
					<button class="blue button" type="submit">Сделать пи***то</button>
				</p>
			</form>
		</div>
		
	<!--Opacity-->
		<div id="css3_opacity" class="t_area">
			<form action="<?=site_url('parse/opacity')?>" method="post">
				<h4>Opacity</h4>
				
				<p>
					<label>Source:</label>
					<textarea name="source" placeholder="opacity: ..."></textarea>
				</p>
				
				<p>
					<label>Result:</label>
					<textarea readonly="readonly" name="result" placeholder="Нет свойств"></textarea>
				</p>

				
				<p>
					<button class="blue button" type="submit">Сделать пи***то</button>
				</p>
			</form>
		</div>
	
	<ul class="footer_tabs">
		
	</ul>
	
	<div class="clear"></div>
	<div class="action_bar">
		<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
		
		<div class="clear"></div>
	</div>
	
</div>