<div class="box-header">
	<h1>Split CSS - разделяй CSS и властвуй</h1>
	<ul>
		<li class="active"><a href="#save_the_world">Спасти мир людей</a></li>
		<li><a href="#about">О проекте</a></li>
		<li><a href="#feedback">Обратная связь</a></li>
	</ul>
</div>

<a title="Splitcss.com" href="<?=site_url('/')?>">
	<img class="logo" src="<?=site_url('img/logo.png')?>" alt="Split CSS" />
</a>
<div class="beta"></div>

<div class="box-content">
	
	<div class="tab-content" id="save_the_world">
		<form method="post" action="<?=site_url('split/check_captcha')?>">
			<h2>Количество запросов в минуту было превышено</h2>
			
			<div class="column-left">
				<div class="marvin-says">
					<div>Вам кажется, что это вы озадачены, но что бы вы делали,
					если бы сами были роботом с маниакально-депрессивным психозом?
					Нет, не трудитесь отвечать.
					Просто введите символы с картинки</div>
					<img src="<?=site_url('img/marvin.png')?>" alt="Marvin" />
				</div>
			</div>
			<div class="column-right">		
				<p><?=$this->recaptcha->get_html('ru')?></p>
				
			</div>
			
			<div class="clear"></div>
			<div class="action_bar">
				
				<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
				
				<button class="button blue">Спасти мир людей</button>
				
				<div class="clear"></div>
			</div>
		</form>
	</div>
	
	<!--О ПРОЕКТЕ-->
		<?=$this->load->view('about_view')?>
		
	<!--ОБРАТНАЯ СВЯЗЬ-->
		<?=$this->load->view('feedback_view')?>
	
</div>
</div>