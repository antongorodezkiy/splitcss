<div class="box-header">
	<h1>Split CSS - разделяй CSS и властвуй</h1>
	<ul>
		<li class="active"><a href="#save_the_world">Спасти мир людей</a></li>
		<li><a href="#about">О проекте</a></li>
		<li><a href="#feedback">Обратная связь</a></li>
	</ul>
</div>

<a class="logo_link" title="Splitcss.com" href="<?=site_url('/')?>">
	<img class="logo" src="<?=site_url('img/logo.png')?>" alt="Split CSS" />
</a>
<div class="beta"></div>

<div class="box-content">
	
	<div class="tab-content" id="save_the_world">
			<h2>Количество запросов в день было превышено</h2>
			
			<div class="column-left">
				
				<p>С вашего адреса сегодня было произведено более 3000 запросов.</p>
				<p>Я не знаю, хорошо это или плохо,
				но на всякий случай доступ с вашего адреса я заблокировал.</p>
				<p>Не беспокойтесь, это буквально до завтра ;)</p>
			</div>
			<div class="column-right">		
				<div class="marvin-says">
					<div>Забавно, что, как только вы начинаете думать,
					будто жизнь, пожалуй, не может стать хуже, она внезапно становится хуже.</div>
					<img src="<?=site_url('img/marvin.png')?>" alt="Marvin" />
				</div>
			</div>
			
			<div class="clear"></div>
			<div class="action_bar">
				
				<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
				
	
				<div class="clear"></div>
			</div>

	</div>
	
	<!--О ПРОЕКТЕ-->
		<?=$this->load->view('about_view')?>
		
	<!--ОБРАТНАЯ СВЯЗЬ-->
		<?=$this->load->view('feedback_view')?>
	
</div>
</div>