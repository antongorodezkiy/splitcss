<div class="tab-content" id="settings">
			  
	<h2>Настройки</h2>
	
	<div class="column-left">
		
		<h4>Разделение css</h4>
		
		<p>
			<input type="checkbox" name="types[]" id="select_colors" value="colors" />
			<label for="select_colors">Отделить цвета</label>
		</p>
		<p>
			<input type="checkbox" name="types[]" id="select_fonts" value="fonts" />
			<label for="select_fonts">Отделить шрифты</label>
		</p>
		<p>
			<input type="checkbox" name="types[]" id="select_images" value="images" />
			<label for="select_images">Отделить изображения</label>
		</p>
		<p>
			<input type="checkbox" name="types[]" id="select_pseudo" value="pseudo" />
			<label for="select_pseudo">Отделить псевдоселекторы</label>
		</p>
		<p>
			<input type="checkbox" name="types[]" id="select_vendors" value="vendors" />
			<label for="select_vendors">Отделить вендорные свойства</label>
		</p>
		<p>
			<input type="checkbox" name="types[]" id="custom_filter" value="custom_filter" />
			<label for="custom_filter">Произвольный фильтр</label>
			<input type="text" name="custom_filter_property_name" placeholder="имя свойства" />
			<input type="text" name="custom_filter_property_value" placeholder="свойство" />
		</p>
		
		
		<h4>Сортировка</h4>
		
		<p>
			<label for="reorder_selectors">Сортировать селекторы</label>
			<select name="reorder_selectors" placeholder="Сортировать селекторы">
				<option value="1">Сортировать</option>
				<option value="0" selected="selected">Оставить как есть</option>
			</select>
		</p>
		
		<p>
			<label for="reorder_properties">Сортировать свойства</label>
			<select name="reorder_properties" placeholder="Сортировать свойства">
				<option value="1">Сортировать</option>
				<option value="0" selected="selected">Оставить как есть</option>
			</select>
		</p>
		
		
		
		
		
	</div>
	
	<div class="column-right">
		
		<h4>Стиль кода</h4>
		
		<p>
			<label for="first_brace_position">Положение открывающей фигурной скобки</label>
			<select name="first_brace_position" placeholder="Положение открывающей фигурной скобки">
				<option value="sameline">На строке селектора</option>
				<option value="newline" selected="selected">На следующей от селектора строке</option>
			</select>
		</p>
		
		<h4>Приоритеты</h4>
		<div class="column-left">
			<p>
				<input type="radio" name="cut_priority" id="cut_priority_selector" value="selector" />
				<label for="cut_priority_selector">Селекторы вырезаются первыми</label>
			</p>
		</div>
		<div class="column-right">
			<p>
				<input selected="selected" type="radio" name="cut_priority" id="cut_priority2" value="property" />
				<label for="cut_priority2">Свойства вырезаются первыми</label>
			</p>
		</div>
		<div class="clear"></div>
		
		<h4>Кроссбраузерность</h4>
		
		<p>
			<input type="checkbox" name="restructure_vendors_properties" id="restructure_vendors_properties" value="1" />
			<label for="restructure_vendors_properties">Реструктурировать вендорные свойства</label>
		</p>
		
		<?php /*<p>
			<input type="checkbox" name="add_css3pie" id="add_css3pie" value="1" />
			<label for="add_css3pie">
				Добавить поддержку CSS3 для Internet Explorer через css3pie
			</label>
		</p>
		
		<p class="warning">
			<a href="http://css3pie.com/">Css3pie</a> это замечательная библиотека
			по излечению недостатков Internet Explorer в погоне за современностью.
			Она добавляет в IE понимание border-radius, box-shadow, linear-gradient и другие плюшки.
			<strong>Рекомендую :)</strong>
		</p>
		
		<p>
			<input type="checkbox" name="add_minmax" id="add_minmax" value="1" />
			<label for="add_minmax">Добавить поддержку min/max-height/width для Internet Explorer с помощью Microsoft&#8217;s dynamic expressions</label>
		</p> */ ?>
		
	</div>
	
	<div class="clear"></div>
	<div class="action_bar">
		
		<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
		
		<div class="clear"></div>
	</div>
</div>