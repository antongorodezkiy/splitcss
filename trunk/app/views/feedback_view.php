<div class="tab-content" id="feedback">
	<h2>Обратная связь</h2>
	<form method="post" action="<?=site_url('split/feedback')?>">
		<div class="column-left">
			<p>
				<label>Ваше имя</label>
				<input type="text" name="name" />
			</p>
			<p>
				<label>Ваш Email</label>
				<input type="text" name="email" />
			</p>
			<p><?=$this->recaptcha->get_html('ru')?></p>
		</div>
		
		<div class="column-right">
			<p>
				<label>Ваше послание</label>
				<textarea name="message" id="" cols="30" rows="10" placeholder="Будьте кратки :)"></textarea>
			</p>
		</div>
		
		<div class="clear"></div>

		<div class="action_bar">
			<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
			
			<button type="submit" class="button blue right">Отправить</button>
			<div class="clear"></div>
		</div>
	</form>
</div>