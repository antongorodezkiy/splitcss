<div class="tab-content" id="source_code">
	<h2>Исходный код</h2>

	<p>
		<label for="source_textarea">Прямой ввод:</label>
		<textarea id="source_textarea" name="source"><?=$this->notify->getData()?></textarea>
	</p>
	
	<p>
		<label for="source_url">Удаленная загрузка:</label>
		<input type="text" id="source_url" name="source_url" placeholder="http://example.com/style.css" />
	</p>

	<p class="source_file_container">
		<label for="source_file">Локальная загрузка:</label>
		<input type="file" name="source_file" id="source_file" value="Загрузка файла .css с вашего жесткого диска" />
	</p>

	<div class="clear"></div>

	<p class="warning">
		Только CSS файлы
	</p>

	<div class="clear"></div>
  
	<div class="action_bar">
		<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
		
		<button class="button blue" id="sources_clear">Очистить</button>
		
		<div class="clear"></div>
	</div>

</div>