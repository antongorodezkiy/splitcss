<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title>Split CSS - разделяй CSS и властвуй</title>
	
	<!-- Stylesheets -->
		<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" href="<?=site_url('css/reset.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/formalize.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/tipsy.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/checkboxes.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/visualize.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/fonts.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/uploadify.css')?>" />
		<link rel="stylesheet" href="<?=site_url('css/main.css')?>" />
		
		<link rel="shortcut icon" href="<?=site_url('img/favicon.ico')?>" type="image/x-icon" />

		<!-- JavaScript -->
		<script src="<?=site_url('js/jquery.min.js')?>"></script>
		<script src="<?=site_url('js/formalize.min.js')?>"></script>
		<script src="<?=site_url('js/jquery.checkboxes.js')?>"></script>
		<script src="<?=site_url('js/jquery.selectskin.js')?>"></script>
		<script src="<?=site_url('js/jquery.inputtags.min.js')?>"></script>
		<script src="<?=site_url('js/jquery.livequery.js')?>"></script>
		<script src="<?=site_url('js/jquery.visualize.js')?>"></script>
		<script src="<?=site_url('js/jquery.tipsy.js')?>"></script>
		
		<script src="<?=site_url('js/swfobject.js')?>"></script>
		<script src="<?=site_url('js/jquery.uploadify.js')?>"></script>

		<?=$this->notify->initJsCss()?>
		<script type="text/javascript">
			var base_url = "<?=base_url();?>";
			var url = [];
			url['parse'] = "<?=site_url('split/parse')?>";
			url['clear'] = "<?=site_url('split/clear')?>";
			url['upload_direct'] = "<?=site_url('upload/direct')?>";
			url['upload_remote'] = "<?=site_url('upload/remote')?>";
		</script>
		<script src="<?=site_url('js/splitcss.js')?>"></script>
		
		
		
</head>
<body>

	<div id="maincontainer">
		<div id="main">
			
			<?=$this->notify->getMessages()?>
		  
			<div class="box">
				<?=$this->load->view($this->showView)?>
			</div>
		
		
		
		</div>
    </div>
	
	
</body>
</html>