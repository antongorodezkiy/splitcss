<div class="box-header">
	<h1>Split CSS - разделяй CSS и властвуй</h1>
	<ul>
		<li class="active"><a href="#source_code">Исходный код</a></li>
		<li><a href="#settings">Настройки</a></li>
		<li><a href="#result_code">Результат</a></li>
		<li><a href="#functions">Функции</a></li>
		<li><a href="#about">О проекте</a></li>
		<li><a href="#feedback">Обратная связь</a></li>
	</ul>
</div>

<a class="logo_link" title="Splitcss.com" href="<?=site_url('/')?>">
	<img class="logo" src="<?=site_url('img/logo.png')?>" alt="Split CSS" />
</a>
<div class="beta"></div>

<div class="box-content">
	<!--ИСХОДНЫЙ КОД-->
		<?=$this->load->view('source_view')?>
	
	<!--НАСТРОЙКИ-->
		<?=$this->load->view('settings_view')?>

	<!--РЕЗУЛЬТАТ-->
		<?=$this->load->view('result_view')?>
		
	<!--ФУНКЦИИ-->	
		<?=$this->load->view('functions_view')?>
	
	<!--О ПРОЕКТЕ-->
		<?=$this->load->view('about_view')?>
		
	<!--ОБРАТНАЯ СВЯЗЬ-->
		<?=$this->load->view('feedback_view')?>
</div>