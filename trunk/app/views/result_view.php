<div class="tab-content results" id="result_code">
	<h2>Результат</h2>
	
	<ul>
		<li><a href="#result">Все, форматировано</a></li>
		<li><a href="#colors">Цвета</a></li>
		<li><a href="#fonts">Шрифты</a></li>
		<li><a href="#images">Изображения</a></li>
		<li><a href="#pseudo">Псевдо</a></li>
		<li><a href="#vendors">Вендорные</a></li>
		<li><a href="#rest">Остальное</a></li>
	</ul>
	
	<p id="result" class="t_area">
		<label for="formated_textarea">Форматированный результат:</label>
		<textarea readonly="readonly" id="formated_textarea" placeholder="Нет свойств"></textarea>
	</p>
	
	<p id="colors" class="t_area">
		<label for="colors_textarea">Свойства цвета:</label>
		<textarea readonly="readonly" id="colors_textarea" placeholder="Нет свойств цвета"></textarea>
	</p>
	
	<p id="fonts" class="t_area">
		<label for="fonts_textarea">Свойства шрифтов:</label>
		<textarea readonly="readonly" id="fonts_textarea" placeholder="Нет свойств шрифтов"></textarea>
	</p>
	
	<p id="images" class="t_area">
		<label for="images_textarea">Свойства изображений:</label>
		<textarea readonly="readonly" id="images_textarea" placeholder="Нет свойств изображений"></textarea>
	</p>
	
	<p id="pseudo" class="t_area">
		<label for="pseudo_textarea">Псевдоселекторы:</label>
		<textarea readonly="readonly" id="pseudo_textarea" placeholder="Нет псевдоселекторов"></textarea>
	</p>
	
	<p id="vendors" class="t_area">
		<label for="vendors_textarea">Вендорные свойства:</label>
		<textarea readonly="readonly" id="vendors_textarea" placeholder="Нет вендорных свойств"></textarea>
	</p>
	
	<p id="rest" class="t_area">
		<label for="rest_textarea">Остальные свойства:</label>
		<textarea readonly="readonly" id="rest_textarea" placeholder="Не осталось свойств после вырезания предыдущих"></textarea>
	</p>
	
	<p id="custom_filtered" class="t_area">
		<label for="filtered_textarea">Отфильтрованные:</label>
		<textarea readonly="readonly" id="filtered_textarea" placeholder="Не осталось свойств после вырезания предыдущих"></textarea>
	</p>
	
	<ul class="footer_tabs">
		<li><a href="#custom_filtered">Custom filter</a></li>
	</ul>
	
	<div class="clear"></div>
	<div class="action_bar">
		<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
		
		<a class="button blue tooltip" title="Скачать заархивированные стили" href="<?=site_url('download/zipped_splited_css')?>">Скачать *.zip</a>
		<a class="button blue tooltip" title="Скачать заархивированные стили, связанные через импорт" href="<?=site_url('download/zipped_splited_with_import_css')?>">Скачать *.zip (@import)</a>
		<a class="button blue tooltip" title="Скачать общий стиль, разбитый по секциям" href="<?=site_url('download/one_splited_css')?>">Скачать splited.css</a>
		<a class="button blue tooltip" title="Скачать общий стиль, форматированный по настройкам" href="<?=site_url('download/one_formated_css')?>">Скачать formated.css</a>
		
		<div class="clear"></div>
	</div>
	
</div>