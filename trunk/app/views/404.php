<div class="box-header">
	<h1>Split CSS - разделяй CSS и властвуй</h1>
	<ul>
		<li class="noajax"><a href="<?=site_url('/')?>">На главную</a></li>
		<li class="active"><a href="#not_found">404</a></li>
		<li><a href="#about">О проекте</a></li>
		<li><a href="#feedback">Обратная связь</a></li>
	</ul>
</div>

<a class="logo_link" title="Splitcss.com" href="<?=site_url('/')?>">
	<img class="logo" src="<?=site_url('img/logo.png')?>" alt="Split CSS" />
</a>
<div class="beta"></div>

<div class="box-content">
	
	<div class="tab-content" id="not_found">
	
			<h2>404 | Страница не найдена</h2>
			
			<div class="column-left">
				<div class="marvin-says">
					<div>Мне кажется, вы должны знать, что я в глубокой депрессии.</div>
					<img src="<?=site_url('img/marvin.png')?>" alt="Marvin" />
				</div>
			</div>
			<div class="column-right">		
				
			</div>
			
			<div class="clear"></div>
			<div class="action_bar">
				
				<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
			
				<div class="clear"></div>
			</div>
	
	</div>
	
	<!--О ПРОЕКТЕ-->
		<?=$this->load->view('about_view')?>
		
	<!--ОБРАТНАЯ СВЯЗЬ-->
		<?=$this->load->view('feedback_view')?>
	
</div>
</div>