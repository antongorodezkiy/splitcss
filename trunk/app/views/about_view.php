<div class="tab-content" id="about">
	<h2>О проекте</h2>
	<hr />
	<p>Этот микро-сервис <a href="/">Splitcss.com</a> с претензией на полезность создан для того, чтобы разделять css-код на группы, такие как изображения, шрифты, цвета и другие.</p>
	<p>Однажды он очень сильно помог мне (правда тогда он был формой с двумя полями и одной кнопкой :) ), возможно будет полезен и вам.</p>
	<p>Некоторые настройки, созданны по советам из статьи <a href="http://higher.com.ua/article/77/70-priemov-uluchsheniya-css-verstki">70 приёмов улучшения css верстки</a>, за что автору сборки и авторам отдельных советов большое спасибо.</p>
	<hr />
	<p>PS Спасибо Thomas Björk за его библиотеку по работе с css, Дугласу Адамсу за цитаты и замечательную литературу, а также Alessandro Rei за изображение Марвина, без них ничего бы не вышло :)</p>
	
	<div class="marvin-says">
		<div>Жизнь! Не говорите мне о жизни...</div>
		<img src="<?=site_url('img/marvin.png')?>" alt="Marvin" />
	</div>
	
	<div class="clear"></div>
	
	<div class="action_bar">
		<a class="copy button" target="_blank" href="http://gorodsideas.nadvoe.org.ua">&#169; 2012, Anton_Gorodezkiy</a>
		<div class="clear"></div>
	</div>
	
</div>