<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parse extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->load->library('cssparser');
		$this->load->library('xcssparser');
		
		$this->xcssparser->removeComments = true;
		
		
		$this->source = $this->input->post('source',true);
		if ($this->source == '' or !strstr($this->source, '{'))
			return false;
    }

	
	/*
	** 
	*/
	public function gradient()
	{
		$this->xcssparser->ParseStr($this->source);
		
		$data = $this->xcssparser->makeCrossbrowserGradient();

		$this->notify->setData($data);
		$this->notify->returnSuccess('CSS разобран и сформирован');
	}
	
	
	/*
	** Border radius
	*/
	public function border_radius()
	{
		// разделять ли на отдельные углы
		$split_per_corner = (bool)$this->input->post('split_per_corner',true);

		$this->xcssparser->ParseStr($this->source);
		
		$data = $this->xcssparser->makeCrossbrowserBorderRadius($split_per_corner);

		$this->notify->setData($data);
		$this->notify->returnSuccess('Углы скруглены, как Вы и просили');
	}
	
	
	/*
	** Text-shadow
	*/
	public function text_shadow()
	{
		$this->xcssparser->ParseStr($this->source);
		
		$data = $this->xcssparser->makeCrossbrowserTextShadow();

		$this->notify->setData($data);
		$this->notify->returnSuccess('Тень текста установлена, как Вы и просили');
	}
	
	
	/*
	** Box-shadow
	*/
	public function box_shadow()
	{
		$this->xcssparser->ParseStr($this->source);
		
		$data = $this->xcssparser->makeCrossbrowserBoxShadow();

		$this->notify->setData($data);
		$this->notify->returnSuccess('Тень блоков установлена, как Вы и просили');
	}
	
	
	/*
	** Opacity
	*/
	public function opacity()
	{
		$this->xcssparser->ParseStr($this->source);
		
		$data = $this->xcssparser->makeCrossbrowserOpacity();

		$this->notify->setData($data);
		$this->notify->returnSuccess('Полупрозрачность установлена, как Вы и просили');
	}
	
}