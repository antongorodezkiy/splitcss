<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	
	function index()
	{
		$this->showView = '404';
		$this->load->view('template');
	}

	
}