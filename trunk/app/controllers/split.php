<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Split extends MY_Controller {

	function __construct()
    {
        parent::__construct();
		
		$this->load->library('recaptcha');
    }
	
	
	
	/*
	** 
	*/
	public function index()
	{
		if (!$this->showView)
		{
			$cached_source = $this->code_db->loadSource();
			
			if ($cached_source)
			{
				$this->notify->setData($cached_source);
				$this->notify->success('Мы Вас узнали :) И загрузили Ваш крайний CSS');
			}
			$this->showView = 'index_view';
		}
		
		$this->load->view('template');
	}
	
	
	
	
	
	/*
	** Основная работа парсера
	*/
	public function parse()
	{

		$this->load->library('cssparser');
		$this->load->library('xcssparser');
		
		$source = $this->input->post('source',true);
	
		if ($source == '' or !strstr($source, '{'))
			return false;
		
		$this->xcssparser->removeComments = true;
		
		$this->xcssparser->ParseStr($source);

		// реструктурировать вендорные свойства
		if ($this->input->post('restructure_vendors_properties',true))
			$this->xcssparser->restructureVendorsProperties();
		
		// настройка открывающей скобки
			$this->xcssparser->setBraceMode( $this->input->post('first_brace_position',true) );
		
		// настройки сортировки
			$sortSelectors = (bool)$this->input->post('reorder_selectors',true);
			$sortProperties = (bool)$this->input->post('reorder_properties',true);
			
			if ($sortSelectors)
				$this->xcssparser->sortSelectors();
				
			if ($sortProperties)
				$this->xcssparser->sortProperties();
	
		// настройки приоритета вырезания
			$cutPriority = $this->input->post('cut_priority',true);
			
	
		$this->types = $this->input->post('types',true);

		// настройки типов
			// возвращается в любом случае
				$this->data['formated'] = $this->xcssparser->getCSS();
			
			// настройки приоритета вырезания
				if ($cutPriority == 'selector')
				{
					$this->parseSelectors();
					$this->parseProperties();
				}
				else
				{
					$this->parseProperties();
					$this->parseSelectors();
				}
			
			// произвольный фильтр 
				if (is_array($this->types) && in_array('custom_filter',$this->types))
					$this->data['filtered'] = $this->xcssparser->getFilteredOnly($this->input->post('custom_filter_property_name',true),$this->input->post('custom_filter_property_value',true));
			
			// остальное
				$this->data['rest'] = $this->xcssparser->getRestOnly();

			// сохранили результаты парсинга и источник в базу
				$this->saveParsedToBase($source,$this->data);

		// отослали результат
			$this->notify->setData($this->data);
			$this->notify->returnSuccess('CSS разобран и сформирован, как Вы и просили');
	}
	
	
	
		function parseSelectors()
		{
			// псевдоселекторы
				if (is_array($this->types) && in_array('pseudo',$this->types))
					$this->data['pseudo'] = $this->xcssparser->getPseudoOnly();
		}
		
		
		function parseProperties()
		{
			// цвета
				if (is_array($this->types) && in_array('colors',$this->types))
					$this->data['colors'] = $this->xcssparser->getColorsOnly();
			
			// шрифты 
				if (is_array($this->types) && in_array('fonts',$this->types))
					$this->data['fonts'] = $this->xcssparser->getFontsOnly();
			
			// изображения и файлы
				if (is_array($this->types) && in_array('images',$this->types))
					$this->data['images'] = $this->xcssparser->getImagesOnly();
					
			// вендорные селекторы
				if (is_array($this->types) && in_array('vendors',$this->types))
					$this->data['vendors'] = $this->xcssparser->getVendorsOnly();
		}
		
		
		
		function saveParsedToBase($source,$data)
		{
			// очистили базу от прошлых результатов
				$this->code_db->deleteAllBySession();
			
			// заполнили свежие результаты
				$fields = array();
				foreach($data as $type_css => $code_css)
				{
					$fields[] = array(
						'session_css' => session_id(),
						'time_css' => time(),
						'code_css' => addslashes(htmlentities($code_css)),
						'type_css' => $type_css
					);
				}
			
				// исходники
					$fields[] = array(
						'session_css' => session_id(),
						'time_css' => time(),
						'code_css' => addslashes(htmlentities($source)),
						'type_css' => 'source'
					);
				
				// добавили все
				return $this->code_db->addAllLoop($fields);
		}
	
	
	
	
	/*
	** Очистка кодов в базе
	*/
	public function clear()
	{
		// очистили базу от прошлых результатов
			if ($this->code_db->deleteAllBySession())
				$this->notify->returnSuccess('Очищено! Теперь можно начать жить, как с чистого листа');
			else
				$this->notify->returnError('Что-то пошло не так, но не беспокойтесь. Скоро все наладится');
	}
	
	
	function check_captcha()
	{
		if ($this->recaptcha->check_answer($this->input->ip_address(),$this->input->post('recaptcha_challenge_field'),$this->input->post('recaptcha_response_field')))
		{
			$this->access_db->unBlockForMinuteLimitExceeded();
			
			$this->notify->returnSuccess('Добро пожаловать в мир Людей!');
		}
		else
		{
			$this->notify->returnError('Не правильно разгадана картинка');
		}
	}
	
	
	function feedback()
	{
		if ($this->recaptcha->check_answer($this->input->ip_address(),$this->input->post('recaptcha_challenge_field'),$this->input->post('recaptcha_response_field')))
		{
			$this->load->library('form_validation');
			
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$message = $this->input->post('message');
			
			$this->form_validation->set_rules('name', 'Ваше имя', 'trim|required');
			$this->form_validation->set_rules('email', 'Ваша почта', 'trim|required|valid_email');
			$this->form_validation->set_rules('message', 'Ваше сообщение', 'trim|required');
			
			if (!$this->form_validation->run())
				$this->notify->returnError($this->form_validation->error_string());
			
			$this->load->library('email');
			
			$this->email->from('feedback@splitcss.com', 'Splitcss');
			$this->email->to('antongorodezkiy@gmail.com');

			$this->email->subject('Splitcss.com: Новый feedback '.date('d.m.Y H:i:s'));
			$this->email->message("Сообщение:\n".$message);
			
			if ($this->email->send())
				$this->notify->returnSuccess('Ваше сообщение отправлено');
			else
				$this->notify->returnError($this->email->print_debugger());

			
		}
		else
		{
			$this->notify->returnError('Не правильно разгадана картинка');
		}
	}
	
}