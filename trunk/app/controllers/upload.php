<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();

    }

	
	private function get_file($source_path)
	{
		$source_css = file_get_contents($source_path);
		
		$source_css = $this->security->xss_clean($source_css);
		
		if ($source_css)
		{
			$this->notify->setData(htmlspecialchars($source_css));
			$this->notify->success('Содержимое файла загружено');
		}
		else
		{
			$this->notify->error('Не удалось получить содержимое файла');
		}
	}
	
	
	function remote()
	{
		$this->load->library('testurl');
		
		$source = $this->input->post('source');
		
		if ($source == '' or !$this->testurl->test($source))
			$this->notify->returnNotify();
		
		$this->get_file($source);
		$this->notify->returnNotify();
	}
	
	
	function direct()
	{
		$_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';
		
		if ( $_FILES['Filedata']['tmp_name'] && !$_FILES['Filedata']['error'] && $_FILES['Filedata']['size'] < (10*1024*1024))
		{
			$this->get_file($_FILES['Filedata']['tmp_name']);
			$this->notify->returnNotify();
		}	
		else
		{
			$this->notify->returnError($this->upload->display_errors());
		}
	}
	
}