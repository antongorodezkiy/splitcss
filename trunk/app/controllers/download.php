<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends MY_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->load->helper('download');
		$this->load->model('Code_model','code_db',true);
		
		$this->prefix_css = "/*
** Splitted css file
**
** formatted by Splitcss.com
** site: http://splitcss.com
**
** time: ".date('d.m.Y H:i:s')."
*/";
    }
	
	
	function makePrefix($type)
	{
		return "
\n\n
/* -----------------------------------*/
/* ---------->>> ".strtoupper($type)." <<<-----------*/
/* -----------------------------------*/
\n";
	}


	/*
	** Общий файл стилей, переформатированный по настройкам
	*/
	function one_formated_css()
	{
		$results = $this->code_db->loadFormated();
		
		$source_css = '';
		foreach($results as $result)
		{
			$source_css .= $this->makePrefix($result['type_css']).$result['code_css']."\n\n";
		}
		
		force_download('formated_(splitcss.com).css', $source_css);
	}
	
	
	/*
	** Общий файл стилей, разбитый по секциям
	*/
	function one_splited_css()
	{
		$results = $this->code_db->loadSplittedResults();
		
		$source_css = '';
		foreach($results as $result)
		{
			$source_css .= $this->makePrefix($result['type_css']).$result['code_css']."\n\n";
		}
		
		force_download('splited_(splitcss.com).css', $source_css);
	}
	
	
	/*
	** Заархивированные скрипты
	*/
	function zipped_splited_css()
	{
		$this->load->library('zip');
		$this->load->helper('file');
			
		// создаем папку внутри архива
			$inzip_path = 'spitted_(splitcss.com)';
			$this->zip->add_dir($inzip_path);
		
		// сохраняем из базы в файлы
			$results = $this->code_db->loadSplittedResults();
			foreach($results as $result)
			{
				// пакуем все файлы в архив
					$this->zip->add_data($inzip_path.'/'.$result['type_css'].'.css', $this->prefix_css."\n\n".$result['code_css']);
			}

		// скачиваем
			$this->zip->download($inzip_path.'.zip');
			
		// подчищаем за собой
		//	delete_files($zipped);
		//	delete_files($temp_path);
	}
	
	
	/*
	** Заархивированные скрипты, подключенные в общий файл через import
	*/
	function zipped_splited_with_import_css()
	{
		$this->load->library('zip');
		$this->load->helper('file');
			
		// создаем папку внутри архива
			$inzip_path = 'spitted_with_import_(splitcss.com)';
			$this->zip->add_dir($inzip_path);
		
		// сохраняем из базы в файлы
			$results = $this->code_db->loadSplittedResults();
			
			$import_directives = '';
			foreach($results as $result)
			{
				$filename = $result['type_css'].'.css';
				// пакуем все файлы в архив
					$this->zip->add_data($inzip_path.'/'.$filename, $this->prefix_css."\n\n".$result['code_css']);
					
				$import_directives .= '@import('.$filename.');';
			}
			
		// основной файл
			$this->zip->add_data($inzip_path.'/all.css', $this->prefix_css."\n\n".$import_directives);

		// скачиваем
			$this->zip->download($inzip_path.'.zip');
			
		// подчищаем за собой
		//	delete_files($zipped);
		//	delete_files($temp_path);
	}

	
}