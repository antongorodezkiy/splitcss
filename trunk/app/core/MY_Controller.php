<?php

/*
** Расширение базового класса модели
*/
class MY_Controller extends CI_Controller
{
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->model('Access_today_model','access_db',true);
		
		$this->showView = '';
		$this->checkIpAccess();
		
		$this->load->model('Code_model','code_db',true);
	}
	
	private function checkIpAccess()
	{
		// статистика текущего ip
		$this->ip_statistics = $this->access_db->loadByIp();		// 1
		
		// если это первый вход
		if (!$this->ip_statistics) 									
			$this->access_db->add();								// 2
		// если пользователь у нас уже был сегодня
		else
		{
			$limitExceeded = $this->limitExceeded();
			if ($limitExceeded=='day')
			{
				if ($this->input->is_ajax_request())
				{
					$this->notify->setComeback('/');
					$this->notify->returnError('Количество запросов в день было превышено');
				}
				
				$this->notify->error('Количество запросов в день было превышено');
				$this->showView = 'day_limit_exceeded_view';
				return false;
				
			}
			elseif ($limitExceeded=='minute' or $limitExceeded=='blocked')
			{
				if ($this->input->is_ajax_request())
				{
					$this->notify->setComeback('/');
					$this->notify->returnError('Количество запросов в минуту было превышено');
				}
				
				
				if ($limitExceeded=='minute')
					$this->access_db->blockForMinuteLimitExceeded(); // 2
				
				
				
				$this->notify->error('Количество запросов в минуту было превышено');
				$this->showView = 'minute_limit_exceeded_view';
				return false;
			}
			else
			{
				$this->access_db->resetMinuteCounter(); 			// 2
				$this->access_db->deleteByExpire(); 				// 3
				$this->access_db->plusCounters();					// 4
				return true;
			}
		}
	}
	
	
	/*
	** Проверка превышения допустимых лимитов
	*/
	private function limitExceeded($day_query_limit = 3000,$minute_query_limit = 60)
	{

		if ($this->ip_statistics)
		{
			// если превышено количество запросов в день или заблокировано за дневное превышение
			if ($this->ip_statistics->per_day_counter_today >= $day_query_limit)
			{
				return 'day';
			}
			// если заблокировано за минутное превышение
			elseif ($this->ip_statistics->minute_block_today)
				return 'blocked';
			// если превышено количество запросов в минуту
			elseif ($this->ip_statistics->per_minute_counter_today >= $minute_query_limit)
				return 'minute';
			else
				return false;
		}
		else
			return false;
	}
	
	
}